﻿using System;
using System.Collections.Generic;

namespace Calculation
{
    public class Calculation
    {
        public static double Calculate(string sum)
        {
            string[] items = sum
                            .Replace("+", " + ")
                            .Replace("-", " - ")
                            .Replace("*", " * ")
                            .Replace("/", " / ")
                            .Replace("(", " ( ")
                            .Replace(")", " ) ")
                            .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // To filter out formula in bracket and perform the calculation first
            List<string> processItems = CalculateBracketValue(items);

            //Then proceed the calculation for multiply and divide
            List<string> process2Items = CalculateMultiplyAndDivide(processItems);

            // Finally only perform the calculation for add and minus.
            double result = CalculateAddAndSubtract(process2Items);

            return result;
        }

        private static List<string> CalculateBracketValue(string[] items)
        {
            List<string> processItems = new List<string>();
            List<string> bracketItems = new List<string>();

            bool foundBracketItems = false;
            int totalBracketOpen = 0;
            int totalBracketClose = 0;
            foreach (var item in items)
            {
                //Start count total open and close bracket
                //If the format of the formula is valid,
                //total count for both open and close bracket will be same.
                if (item == "(")
                {
                    foundBracketItems = true;
                    totalBracketOpen += 1;
                }
                if (item == ")") totalBracketClose += 1;

                //if detected bracket, will try to tracking formulat inside the bracket
                if (foundBracketItems)
                {
                    bracketItems.Add(item);

                    // when totalBracketOpen = totalBracketClose mean a group of nested formula has make up.
                    // Will pass in function Calculate() to calculate value for the nested formula
                    if (totalBracketOpen == totalBracketClose)
                    {
                        string formula = "";
                        for (int i = 1; i < bracketItems.Count - 1; i++)
                            formula += (formula != "" ? " " : "") + bracketItems[i];

                        var result = Calculate(formula);
                        processItems.Add(result.ToString());

                        foundBracketItems = false;
                        totalBracketOpen = 0;
                        totalBracketClose = 0;
                        bracketItems.Clear();
                    }
                }
                else
                    processItems.Add(item);
            }

            //When totalBracketOpen != totalBracketClose, mean detect invalid format for bracket
            if (totalBracketOpen > 0 && totalBracketClose > 0 && totalBracketOpen != totalBracketClose)
                throw new Exception("The format for bracket is invalid!");

            return processItems;
        }

        private static List<string> CalculateMultiplyAndDivide(List<string> processItems)
        {
            //Looping to look for value for Multiply and Divide
            List<string> newProcessItems = new List<string>();

            int index = 0;
            string previous_value = "";

            while (index <= processItems.Count - 1)
            {
                string current_value = processItems[index];

                if (current_value == "*" || current_value == "/")
                {
                    // When detected operator is multiply or divide,
                    // will look for previous and next value to perform multiply/divide calculation.
                    double number1 = double.Parse(previous_value);
                    double number2 = double.Parse(processItems[index + 1]);
                    current_value = (current_value == "*" ? number1 * number2 : number1 / number2).ToString();
                    index += 2;
                }
                else if (current_value == "+" || current_value == "-")
                {
                    // When detected operatior but not multiply or divide,
                    // mean it will be Add or Minus,
                    // record the value in the list and return out for other function usage.
                    newProcessItems.Add(previous_value);
                    newProcessItems.Add(current_value);
                    index += 1;
                }
                else
                    index += 1;

                if (index == processItems.Count)
                {
                    double number1 = 0;
                    //handling last value not recorded in the list
                    //if last value is not number, mean the formula format is invalid
                    if (double.TryParse(current_value, out number1))
                        newProcessItems.Add(current_value);
                    else
                        throw new Exception("The formula format is invalid!");
                }
                else
                    previous_value = current_value;
            }

            return newProcessItems;
        }

        private static double CalculateAddAndSubtract(List<string> processItems)
        {
            double result = 0;
            string previous_operator = "";
            int iCount = 0;
            foreach (var item in processItems)
            {
                //Start perform add and minus calculation
                //If last value is not number, mean the formula format is invalid
                iCount += 1;
                double number = 0;
                bool isNumber = double.TryParse(item, out number);
                if (item == "+" || item == "-")
                    previous_operator = item;
                else if (isNumber)
                    result += (previous_operator == "+" || previous_operator == "" ? number : (-1) * number);
                else if (iCount == processItems.Count)
                    if (!isNumber)
                        throw new Exception("The formular format is invalid!");
            }

            return result;
        }
    }
}